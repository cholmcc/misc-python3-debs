#!/bin/bash

mess()
{
    echo $@ > /dev/stderr
}

log()
{
    local f=$1
    if test -f $f ; then
	echo > /dev/stderr
	echo "=== Content of log file ${f} ===" > /dev/stderr
	cat $f > /dev/stderr
	echo "=== End of content of ${f} ===" > /dev/stderr
    fi
}

fail()
{
    mess "Failed!"
    mess $@
    exit 1
}

      
download()
{
    mess -n "Downloading $n ... "
    n=$1
    d=$2
    
    t=`pypi-download $n 2>${d}.down.log`
    if test x"$t" = x ; then
	log ${d}.down.log
	fail "Pip $n not found"
    fi
    mess "done"
    echo "$t" | sed 's/OK: //'
}

setup()
{
    local t=$1 ; shift
    local d=$1 ; shift
    local v=$1 ; shift 
    mess -n "Set-up package directory from ${t}..."

    opt="\
	--with-python3 True \
	--with-python2 False \
	--extra-cfg-file stdeb.cfg"
    if test -f debian/${d}.patch ; then
    	#mess -n "patch with ${d} ... "
	#opt="${opt} -p debian/${d}.patch -l 1"
	:
    fi

    
    py2dsc \
	${opt} \
	$@ ${t} > ${d}.src.log 2>&1
    if test $? -ne 0 || test ! -d deb_dist/${d}-${v}/debian ; then
	log ${d}.src.log
	fail "Failed to make source directory"
    fi

    mess -n "copy from debian/ ... "
    for i in postinst prerm ; do
	if test -f debian/python3-${d}.${i} ; then
	    mess -n "${d}.${i} ... "
	    cp debian/python3-${d}.${i} deb_dist/${d}-${v}/debian/
	fi
    done
    rm -rf deb_dist/${d}-${v}/debian/patches
    for i in debian/*${d}.patch ; do
	mess -n `basename $i .patch`
	mkdir -p deb_dist/${d}-${v}/debian/patches/
	cp ${i} deb_dist/${d}-${v}/debian/patches/
	echo `basename $i` >>  deb_dist/${d}-${v}/debian/patches/series
    done 
    
    mess "done"
}

build()
{
    mess -n "Building ..."
    d=$1
    v=$2

    (cd deb_dist/${d}-${v} && dpkg-buildpackage -rfakeroot -us -uc) \
	> ${d}.build.log 2>&1
    if test $? -ne 0 ; then
	log ${d}.build.log
	fail "Failed to build package"
    fi

    mess "done"
}

n=$1 ; shift
if test x$n = x"clean" ; then
    mess -n "Cleaning ... "
    rm -f *.log *.tar.gz
    rm -rf deb_dist tmp
    mess "done"
    exit 0
fi
    

d=`echo "$n" | tr '_' '-'`
rm -f ${d}.*.log

t=`download "$n" "$d"`
if test "x$t" = "x" ; then exit 1; fi
v=`ls ${t} | sed 's/.*-\([0-9.][0-9.]*\)\.tar\.gz/\1/'`

setup "$t" "$d" "$v" $@
build "$d" "$v"

mess "Logs in ${d}.*.log"

