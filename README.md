# Packaging scripts for various Python3 packages 

Here are some packaging scripts for various Python3 packages currently
not in Debian but available from PIP.

The script `doit.sh` is the main thing, and the configuration for each
package is stored in `stdeb.cfg`.  The directory `debian` contains
additional files to be put into the `debian` directory of the
package. 

we use the [stdeb](https://github.com/astraw/stdeb) utility for the
semi-automatic conversion. The file `stdeb.cfg` configures that tool 

The script `doit.sh` does, given a [PyPi](https://pypi.org) package
name

1. Downloads the source package from [PyPi](https://pypi.org)
2. Figure out the filename of the source archive, and the package name
   and version. 
3. Run the tool `py2dsc` to create the source package
4. Copy any files from `debian` to the package directory 
   * These can for example be `.postinst` or `.preinst` files 
   * It can also be patches.  These will be copied to `debian/patches`
     and an appropriate `series` file is written.  Note, the order of
     the patch names is the same order the patches will be applied, so
     a good idea is to name them something like `NN-package.path`
     where `NN` is a serial number, and `package` is the package
     name. 
 5. Build the binary package using `dpkg-buildpackage` 
 
 ## Configured packages 
 
- `jupyter_highlight_selected_word`
- `jupyter_contrib_core`
- `jupyter_latex_envs`
- `jupyter_contrib_nbextensions`
- `jupyter_nbextensions_configurator`
- `nbgrader`

Copyright (c) 2019 Christian Holm Christensen
